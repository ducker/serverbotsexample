//
//  main.m
//  ServerBotsExample
//
//  Created by Leszek Kaczor on 04/07/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([UKAppDelegate class]));
    }
}
