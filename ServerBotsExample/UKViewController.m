//
//  UKViewController.m
//  ServerBotsExample
//
//  Created by Leszek Kaczor on 04/07/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import "UKViewController.h"
#import <UKResponseParser.h>

@interface UKViewController ()

@end

@implementation UKViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [UKResponseParser parseResponse:nil usingClass:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
