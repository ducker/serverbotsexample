//
//  UKAppDelegate.h
//  ServerBotsExample
//
//  Created by Leszek Kaczor on 04/07/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
